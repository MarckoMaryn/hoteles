$(function(){
    		$("[data-toggle='tooltip']").tooltip();
    		$("[data-toggle='popover']").popover();
    		/* Tiempo manual del carousel*/
    		$('.carousel').carousel({
    			interval:2000
    		});

             /* conceptos basicos de jquery*/
    		 $('#contacto').on('show.bs.modal', function (e) {
                      console.log("Comenzo a abriri el modal");
                 });
    		 /* conceptos cambiar color y desactivar cuando se active el modal
    		     Solo sobre el hotel Buenos aires*/
    		 $('#contacto').on('shown.bs.modal', function (e) {
                      console.log("se termino de abrir el modal");
                      $('#contactoBTN').removeClass('btn-outline-success');
                       $('#contactoBTN').addClass('btn-primary');
                       $('#contactoBTN').prop('disabled',true);
                 });
    		 $('#contacto').on('hide.bs.modal', function (e) {
                      console.log("Comenzo a cerrar el modal");
                 });
    		 $('#contacto').on('hidden.bs.modal', function (e) {
                      console.log("se termino de cerrar el modal");
                      $('#contactoBTN').removeClass('btn-primary');
                       $('#contactoBTN').addClass('btn-outline-success');
                       $('#contactoBTN').prop('disabled',false);
                 });

    	});
